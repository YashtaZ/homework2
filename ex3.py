#!/usr/bin/env python3
"""Kacper Wenta"""

class Flower:
    """Class creating Flower type object"""
    def __init__(self, flower_name, color, size):
        """Ctor"""
        self.flower_name = flower_name
        self.color = color
        self.size = size

    def __repr__(self):
        """Repr"""
        return f"{self.flower_name} {self.color} {self.size}"

def get_number():
    """Function getting and converting values"""
    a = input("Give number: ")
    a = int(a)
    print(a)
    print(isinstance(a, int))


name: str = input("Name: ")
surname: str = input("Surname: ")
age: int = input("age: ")
print(name, surname, age)

get_number()
aa = input("Name of the flower: ")
bb = input("Color of the flower: ")
cc = input("Size of the flower: ")
flower = Flower(aa, bb, cc)
print(flower)
