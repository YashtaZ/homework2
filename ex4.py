#!/usr/bin/env python3
"""Kacper Wenta"""

class Rectangle:
    """Class creating a sphere type object"""
    def __init__(self, name, a, b):
        """Ctor"""
        self.name = name
        self.a = a
        self.b = b
        self.field = a*b

    def __eq__(self, other):
        return self.field == other.field

    def __lt__(self, other):
        return self.field < other.field

    def __gt__(self, other):
        return self.field > other.field

    def __le__(self, other):
        return self.field <= other.field

    def __ge__(self, other):
        return self.field >= other.field

    def __ne__(self, other):
        return self.field != other.field

    def __repr__(self):
        return f"{self.name} {self.field}"


class Sphere:
    """Class creating a sphere"""
    def __init__(self, name, r):
        """Ctor"""
        self.name = name
        self.r = r
        self.volume = 4/3*3.14*(r*r*r)

    def __eq__(self, other):
        return self.volume == other.volume

    def __lt__(self, other):
        return self.volume < other.volume

    def __gt__(self, other):
        return self.volume > other.volume

    def __le__(self, other):
        return self.volume <= other.volume

    def __ge__(self, other):
        return self.volume >= other.volume

    def __ne__(self, other):
        return self.volume != other.volume

    def __repr__(self):
        return f"{self.name} {self.volume}"

class Family:
    def __init__(self, f_name):
        self.f_name = f_name

    def __eq__(self, other):
        return self.f_name == other.f_name

    def __lt__(self, other):
        return self.f_name < other.f_name

    def __gt__(self, other):
        return self.f_name > other.f_name

    def __le__(self, other):
        return self.f_name <= other.f_name

    def __ge__(self, other):
        return self.f_name >= other.f_name

    def __ne__(self, other):
        return self.f_name != other.f_name


class Sibling(Family):
    def __init__(self, *args):
        super(Sibling, self).__init__(*args)


class DadSon(Family):
    def __init__(self, *args):
        super(Sibling, self).__init__(*args)

class Sister(Sibling):
    def __init__(self, *args):
        super(Sibling, self).__init__(*args)

class Brother(Sibling):
    def __init__(self, *args):
        super(Sibling, self).__init__(*args)

class Aunt(Family):
    def __init__(self, *args):
        super(Sibling, self).__init__(*args)


def are_names_equal():
    name = input("Give name: ")
    if name == "Kacper":
        print("Hello Kacper")
    else:
        print("Hello stranger")


#aa = Rectangle("A", 5, 5)
#bb = Rectangle("B", 10, 10)
#c = Sphere("C", 5)
#d = Sphere("D", 10)
#print(aa < bb)
#print(c > d)

sib = Sibling("Jacek")
asa = Brother("Jaceka")
print(sib != asa)
