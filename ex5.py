#!/usr/bin/env python3
"""Kacper Wenta"""

import random

x = 0
while x != 20:
    x = random.randrange(10, 21, 1)
    print(x)

for counter in range(20, -1, -1):
    print(counter)

list_a = [
    [1],
    [2],
    [3]
]
list_b = list()
list_c = [3.14, 2.23, 2.45, 3.12, 5.12]

for number in range(100, 116, 1):
    list_b.append(number)

for numbers in list_c:
    print(numbers)

print(list_b)
