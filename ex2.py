#!/usr/bin/env python3
"""Kacper Wenta"""

class Person:
    """Class creating a Person type object"""

    def __init__(self, name, surname, experience):
        """Ctor"""
        self.name = name
        self.surname = surname
        self.experience = experience

    def __repr__(self) -> str:
        """Repr"""
        return f"{self.name} {self.surname} {self.experience} yrs"


class OfficeWorker(Person):
    """Class creating an OfficeWorker"""
    def __init__(self, position, *args, **kwargs):
        """Ctor"""
        super(OfficeWorker, self).__init__(*args, **kwargs)
        self.position = position

    def __repr__(self, *args, **kwargs):
        rep = super(OfficeWorker, self).__repr__()
        return f"{rep} {self.position}"


boss = OfficeWorker(name="Kacper", surname="Wenta", position="Boss", experience="30")
human_being = Person("Maciek", "Mackowski", 23)
print(isinstance(boss, Person))
print(boss)
print(human_being)
