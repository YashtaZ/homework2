#!/usr/bin/env python3

"""Kacper Wenta"""

class Furniture:
    """Class creating a Furniture object"""
    pass


class OfficeFurniture(Furniture):
    """Class creating a OfficeFurniture object"""
    pass


class Desk(OfficeFurniture):
    """Class creating a Desk object"""
    pass


def check_my_name():
    """Function created to check my name"""
    return print("Kacper")


def eq():
    """Function created to check this simple math equation"""
    sums = 5+3+7
    return print(sums)


def hello_world():
    """Function returning Hello World"""
    return "Hello World"


def minus(a: int, b: int) -> int:
    """Function created to subtract 2 given numbers"""
    return a-b


def new_object() -> Desk:
    """Function Creating an object of type Desk"""
    return Desk()


check_my_name()
eq()
print(hello_world())
print(minus(3, 1))
ayap = new_object()
print(isinstance(ayap, Desk))
